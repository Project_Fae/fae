﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShiftBlue : MonoBehaviour {

    private bool blueActive;

    private void Start()
    {
        blueActive = false;
        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(false);
        }
    }

    private void OnEnable()
    {
        EventManager.OnShift += BlueShift;
    }

    private void OnDisable()
    {
        EventManager.OnShift -= BlueShift;
    }

    void BlueShift()
    {
        if (blueActive)
        {
            foreach (Transform child in transform)
            {
                child.gameObject.SetActive(false);
            }
            blueActive = false;
        }
        else
        {
            foreach (Transform child in transform)
            {
                child.gameObject.SetActive(true);
            }
            blueActive = true;
        }
    }
}
