﻿//Rob, Megan, and Evan
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player_Actions : MonoBehaviour {

    //Public Members
    public CameraRaycastForward rayCaster;
    public EventManager eventManager;
    public Dissolver_Manager dissolverManager;
    public float maxInteractDist = 1.5f;


    //Private Members
    private float _pauseTimer = 0;
    private float _shiftTimer = 0;
    private float _pickUpTimer = 0;
    private float _interactTimer = 0;
    private float _buttonCycle = 0.3f;

    //Updates once per frame
    private void Update(){
        _TrackTime();
    }

    /// <summary>
    /// Keeps track of timers for each various action
    /// </summary>
    private void _TrackTime(){
        if(_pauseTimer > 0){
            _pauseTimer -= Time.deltaTime;
        }
        if (_shiftTimer > 0){
            _shiftTimer -= Time.deltaTime;
        }
        if (_pickUpTimer > 0){
            _pickUpTimer -= Time.deltaTime;
        }
        if (_interactTimer > 0){
            _interactTimer -= Time.deltaTime;
        }
    }

    public void Pause(string pause){
        if (Input.GetAxis(pause) > 0 && _pauseTimer <= 0){
            Debug.Log("Attempting to pause...");
            _pauseTimer = _buttonCycle;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            SceneManager.LoadScene("MainMenu");
        }
    }

    public void ChangeWorlds(string shift){
        if (Input.GetAxis(shift) > 0 && _shiftTimer <= 0){
            Debug.Log("Attempting to shift...");
            eventManager.Shift();
            //dissolverManager.ChangeWorlds();
            _shiftTimer = _buttonCycle;
        }
    }

    public void PickUp(string pick){
        if (Input.GetAxis(pick) > 0 && _pickUpTimer <= 0){
            Debug.Log("Attempting to pick up...");
            _pickUpTimer = _buttonCycle;
        }
    }

    public void Interact(string interact){
        if (Input.GetAxis(interact) > 0 && _interactTimer <= 0){
            Debug.Log("Attempting to interact...");
            GameObject thing = rayCaster.GetObject(maxInteractDist);
            if (thing != null){
                Debug.Log(thing.name);
                if (thing.tag == "Interactable"){
                    thing.GetComponent<Switch>().Interact();
                }
                else{
                    Debug.Log("Tag: " + thing.tag);
                }
            }
            _interactTimer = _buttonCycle;
        }
    }
}
