﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Input_Manager : MonoBehaviour {

    //Public Members
    public Player_Movement playerMove;
    public Player_Actions playerAcitons;
    public bool controllerPresent = false;

    //Private Members
        //Input String Members
        private string _os = "";
        private string _moveX = "";
        private string _moveY = "";
        private string _lookX = "";
        private string _lookY = "";
        private string _pause = "";
        private string _shift = "";
        private string _changeCameraPos = "";
        private string _jump = "";
    public string _pickUp = "";
    public string _interact = "";
    private bool _moveEnabled = true;
    private bool _lookEnabled = true;
    private bool _actionEnabled = true;
    private bool _jumpEnabled = true;



    // Use this for initialization
    void Start () {
        _DetectOS();
        _DetectControllers();
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    private void FixedUpdate(){
        //Allows the player to move
        if (controllerPresent){
            if (_moveEnabled){
                playerMove.MoveXBOX(_moveX, _moveY);
            }
            if (_lookEnabled){
                playerMove.LookXBOX(_lookX, _lookY);
            }
        }
        else{
            if (_moveEnabled){
                playerMove.Move();
            }
            if (_lookEnabled){
                playerMove.Look();
            }
        }

        if (_jumpEnabled){
            playerMove.Jump(_jump);
        }

        //Allows the player to perform actions
        if (_actionEnabled){
            playerAcitons.Pause(_pause);
            playerAcitons.ChangeWorlds(_shift);
            playerAcitons.PickUp(_pickUp);
            playerAcitons.Interact(_interact);
        }
    }

    private void _DetectOS(){
        if (SystemInfo.operatingSystemFamily == OperatingSystemFamily.MacOSX){
            _os = "Mac";
            Debug.Log("OS = Mac");
        }
        else if (SystemInfo.operatingSystemFamily == OperatingSystemFamily.Windows){
            _os = "Windows";
            Debug.Log("OS = Windows");
        }
        else if (SystemInfo.operatingSystemFamily == OperatingSystemFamily.Linux){
            _os = "Linux";
            Debug.Log("OS = Linux");
        }
    }

    public void _DetectControllers(){
        string[] controllers = Input.GetJoystickNames();
        if (controllers.Length > 0){
            Debug.Log("There's a controller present!");
            AssignControllerInputStrings();
            controllerPresent = true;
        }
        else{
            Debug.Log("There's no controller present!");
            controllerPresent = false;
            AssignKeyboardInputStrings();
        }
    }

    public void AssignControllerInputStrings(){
        if (_os == "Mac"){
            _moveX = "Horizontal_XBOX_Mac";
            _moveY = "Vertical_XBOX_Mac";
            _lookX = "MouseX_XBOX_Mac";
            _lookY = "MouseY_XBOX_Mac";
            _pause = "Start_XBOX_Mac";
            _shift = "LeftBumper_XBOX_Mac";
            _changeCameraPos = "RightStick_XBOX_Mac";
            _jump = "B_XBOX_Mac";
            _pickUp = "A_XBOX_Mac";
            _interact = "Y_XBOX_Mac";
        }
        else if (_os == "Windows"){
            _moveX = "Horizontal_XBOX_Windows";
            _moveY = "Vertical_XBOX_Windows";
            _lookX = "MouseX_XBOX_Windows";
            _lookY = "MouseY_XBOX_Windows";
            _pause = "Start_XBOX_Windows";
            _shift = "LeftBumper_XBOX_Windows";
            _changeCameraPos = "RightStick_XBOX_Windows";
            _jump = "B_XBOX_Windows";
            _pickUp = "A_XBOX_Windows";
            _interact = "Y_XBOX_Windows";
        }
        else if (_os == "Linux"){
            _moveX = "Horizontal_XBOX_Linux";
            _moveY = "Vertical_XBOX_Linux";
            _lookX = "MouseX_XBOX_Linux";
            _lookY = "MouseY_XBOX_Linux";
            _pause = "Start_XBOX_Linux";
            _shift = "LeftBumper_XBOX_Linux";
            _changeCameraPos = "RightStick_XBOX_Linux";
            _jump = "B_XBOX_Linux";
            _pickUp = "A_XBOX_Linux";
            _interact = "Y_XBOX_Linux";
        }
        else{
            Debug.Log("There was some kind of error in detecting the OS...");
            AssignKeyboardInputStrings();
        }
    }

    public void AssignKeyboardInputStrings(){
        _moveX = "Horizontal";
        _moveY = "Vertical";
        _lookX = "Mouse X";
        _lookY = "Mouse Y";
        _pause = "Escape";
        _shift = "E";
        _changeCameraPos = "Tab";
        _jump = "Space";
        _pickUp = "Left Click";
        _interact = "Right Click";
    }

    /// <summary>
    /// Enables or disables ALL input.
    /// </summary>
    /// <param name="b">If set to <c>true</c> b.</param>
    public void EnablePlayer(bool b){
        _moveEnabled = b;
        _lookEnabled = b;
        _actionEnabled = b;
        _jumpEnabled = b;
    }


    /// <summary>
    /// Enables or disables walking.
    /// </summary>
    /// <param name="b">If set to <c>true</c> b.</param>
    public void EnableMovement(bool b){
        _moveEnabled = b;
    }

    /// <summary>
    /// Enables or disables jumping.
    /// </summary>
    /// <param name="b">If set to <c>true</c> b.</param>
    public void EnableJump(bool b){
        _jumpEnabled = b;
    }

    /// <summary>
    /// Enables or disables looking.
    /// </summary>
    /// <param name="b">If set to <c>true</c> b.</param>
    public void EnableLook(bool b){
        _lookEnabled = b;
    }

    /// <summary>
    /// Enables or disables ALL action input.
    /// </summary>
    /// <param name="b">If set to <c>true</c> b.</param>
    public void EnableAction(bool b){
        _actionEnabled = b;
    }

    private void OnTriggerEnter(Collider other){
        if (other.tag == "Enemy"){
            other.gameObject.GetComponent<Enemy_Movement_Transform_Field>().aware = true;
        }
    }
}
