﻿//Rob, Emilio, Evan
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Movement : MonoBehaviour {

    //Public Members
    public Rigidbody rb;
        //Move Members
        public GameObject mainCamera;
        public float moveSpeed = 2;
        //Look Members
        public float viewRange = 80;
        public float sensitivity = 2;
    //Jump Members
    public int currentJumps = 0;
    public int maxJumps = 1;
    public float jumpForce = 4;

    //Jump Members
    public int numJumps = 1;


    //Private Members
        //Look Members
        private float _yaw = 0.0f;
        private float _pitch = 0.0f;


    public void Move(){
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.transform.Translate(movement * moveSpeed * Time.deltaTime);
    }

    public void MoveXBOX(string moveX, string moveY){
        float moveHorizontal = Input.GetAxis(moveX);
        float moveVertical = -Input.GetAxis(moveY);

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.transform.Translate(movement * moveSpeed * Time.deltaTime);
    }


    public void Look(){
        _yaw += sensitivity * Input.GetAxis("Mouse X");
        _pitch -= sensitivity * Input.GetAxis("Mouse Y");

        _pitch = Mathf.Clamp(_pitch, -viewRange, viewRange);

        transform.eulerAngles = new Vector3(0, _yaw, 0);
        mainCamera.transform.eulerAngles = new Vector3(_pitch, _yaw, 0);
    }

    public void LookXBOX(string lookX, string lookY){
        _yaw += sensitivity * Input.GetAxis(lookX);
        _pitch += sensitivity * Input.GetAxis(lookY);

        _pitch = Mathf.Clamp(_pitch, -viewRange, viewRange);

        transform.eulerAngles = new Vector3(0, _yaw, 0);
        mainCamera.transform.eulerAngles = new Vector3(_pitch, _yaw, 0);
    }

    public void Jump(string jump){
        if(Input.GetAxis(jump) > 0 && currentJumps > 0){
            currentJumps--;
            Vector3 vector = new Vector3(0, jumpForce, 0);
            rb.AddForce(vector);
            Debug.Log("Jump!");
        }
    }

    private void OnCollisionEnter(Collision collision){
        if(collision.gameObject.tag == "Ground"){
            currentJumps = maxJumps;
        }
    }
}
