﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Switch : Interactable {
    private GameObject me;
    private bool activated = false;
    private Light light1;
    public GameObject player;
    private Text interact;
    public Door[] doors = new Door[10];
    private bool interactable = false;
    [SerializeField]
    AudioSource switchnoise;

	// Use this for initialization
	void Start () {
        me = this.gameObject;
        player = GameObject.FindGameObjectWithTag("Player");
        doors = me.GetComponentsInParent<Door>();
        light1 = me.GetComponent<Light>();
        //interact = GameObject.FindGameObjectWithTag("Message").GetComponent<Text>();
        EventManager.OnShift += Disable;
	}

	////Keeping this incase my edits don't work
	//// Update is called once per frame
	//void Update () {
 //       if (interactable)
 //       {
 //           if (Input.GetButtonDown("Left Click"))
 //           {
 //               if (activated)
 //               {
 //                   foreach(Door door in doors)
 //                   door.Switch();
 //                   activated = false;
 //                   light1.color = Color.red;
 //                   Debug.Log("Switch deactivated");
 //               }
 //               else if (!activated)
 //               {
 //                   foreach(Door door in doors)
 //                   door.Switch();
 //                   activated = true;
 //                   light1.color = Color.blue;
 //                   Debug.Log("Switch activated");
 //               }
 //               switchnoise.Play();

 //           }
 //       }
	//}

    public override void Interact(){
        Debug.Log("Interacted with Switch");
        if (activated){
            foreach (Door door in doors){
                Debug.Log("Switching door");
                door.Switch();
            }
            activated = false;
            light1.color = Color.red;
            Debug.Log("Switch deactivated");
        }
        else if (!activated){
            foreach (Door door in doors){
                Debug.Log("Switching door");
                door.Switch();
            }
            activated = true;
            light1.color = Color.blue;
            Debug.Log("Switch activated");
        }
        switchnoise.Play();

    }

    public void OnTriggerEnter(Collider other){
        if (other.tag.Equals("Player")){
            //interact.enabled = true;
            interactable = true;
        }
    }

    public void OnTriggerExit(Collider other){
        if (other.tag.Equals("Player")){
            //interact.enabled = false;
            interactable = false;
        }
    }

    public void Disable()
    {
        if (interactable)
        {
            interactable = false;
            interact.enabled = false;
        }
    }
}
