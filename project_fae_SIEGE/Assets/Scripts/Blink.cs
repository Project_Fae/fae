﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Blink : MonoBehaviour
{

    public GameObject enemyGO;
    public GameObject playerGO;

    [SerializeField]
    private Image eyeblu;
    [SerializeField]
    private Image eyered;
    [SerializeField]
    private Text timer;
    private float uitime;
    private bool red = true;
    // Use this for initialization
    void Start()
    {
        EventManager.OnShift += BlinkAnim;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time - uitime >= .2f)
        {
            timer.enabled = false;
            //Debug.Log("fae time text disabled, timer ended");
        }
    }

    private void BlinkAnim()
    {
        if (red)//Blue
        {
            timer.enabled = true;
            uitime = Time.time;
            //Debug.Log("Fae time text enabled, timer set");
            red = false;
            eyered.transform.SetSiblingIndex(1);
            eyeblu.transform.SetSiblingIndex(0);
            eyered.color = new Color(eyered.color.r, eyered.color.g, eyered.color.b, 1);
            eyered.CrossFadeColor(eyeblu.color, 0f, false, true);
            eyeblu.CrossFadeAlpha(1, 0.1f, false);
            StartCoroutine(FadeOutShort(eyered));
            StartCoroutine(FadeOutLong(eyeblu));
            enemyGO.SetActive(true);
            //Vector3 newPos = playerGO.transform.position;
            //newPos.y++;
            //enemyGO.transform.position = newPos;
        }
        else if (!red)//Red
        {
            timer.enabled = true;
            uitime = Time.time;
            //Debug.Log("Fae time text enabled, timer set");
            enemyGO.SetActive(false);
            red = true;
            eyeblu.transform.SetSiblingIndex(1);
            eyered.transform.SetSiblingIndex(0);
            eyeblu.color = new Color(eyeblu.color.r, eyeblu.color.g, eyeblu.color.b, 1);
            eyeblu.CrossFadeColor(eyered.color, 0f, false, true);
            eyered.CrossFadeAlpha(1, 0.1f, false);
            StartCoroutine(FadeOutShort(eyeblu));
            StartCoroutine(FadeOutLong(eyered));
        }
    }

    IEnumerator FadeOutLong(Image e)
    {
        yield return new WaitForSeconds(0.2f);
        e.CrossFadeAlpha(0, 0.2f, false);
    }

    IEnumerator FadeOutShort(Image e)
    {
        yield return new WaitForSeconds(0.1f);
        e.CrossFadeAlpha(0, 0.1f, false);
    }

}
