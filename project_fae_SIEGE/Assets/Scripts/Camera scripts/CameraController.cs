﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject camPoint1;
    public GameObject camPoint2;
    public GameObject frontView;
    public GameObject playerGO;
    bool camPointOption = true;
    bool front = false;

    private Vector3 fv;

	// Use this for initialization
	void Start ()
    {
        fv = frontView.transform.position;
        this.gameObject.transform.position = camPoint1.transform.position;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.V) && camPointOption == true)
        {
            camPointOption = false;
        }
        else if (Input.GetKeyDown(KeyCode.V) && camPointOption == false)
        {
            camPointOption = true;
        }

        if(Input.GetKeyDown(KeyCode.Backslash)){
            front = !front;

            if (front){
                this.gameObject.transform.position = fv;
            }
            else{
                this.gameObject.transform.position = camPoint1.transform.position;
            }
        }

        if (camPointOption == false)
        {
            this.gameObject.transform.position = camPoint2.transform.position;
        }

        if (camPointOption == true)
        {
            this.gameObject.transform.position = camPoint1.transform.position;
        }
	}
}
