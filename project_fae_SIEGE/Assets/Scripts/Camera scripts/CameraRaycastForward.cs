﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRaycastForward : MonoBehaviour {

    public GameObject GetObject(float maxDistance){
        Vector3 position = gameObject.transform.position;
        RaycastHit hit;
        Vector3 target = Camera.main.transform.forward * maxDistance;
        if (Physics.Raycast(position, target, out hit)){
            return hit.collider.gameObject;
        }
        else{
            return null;
        }
    }
}
