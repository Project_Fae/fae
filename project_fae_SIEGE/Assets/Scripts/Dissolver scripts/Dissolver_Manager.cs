﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dissolver_Manager : MonoBehaviour {

    public List<Dissolver> dissolvers_ = new List<Dissolver>();
    public Dissolver[] dissolvers;
    public GameObject[] faeObjects;
    public GameObject[] ruinObjects;
    public GameObject[] interactableObjects;

    public Texture faeTexture;
    public Texture ruinTexture;
    public bool switched = false;

	// Use this for initialization
	void Start () {
        dissolvers = GameObject.FindObjectsOfType<Dissolver>();
        //faeObjects = GameObject.FindGameObjectsWithTag("Fea");
        //ruinObjects = GameObject.FindGameObjectsWithTag("Ruin");
        //interactableObjects = GameObject.FindGameObjectsWithTag("Interactable");

    }

    private void Update(){

    }


    public void ChangeWorlds(){
        if (!switched){
            _UnDissolve();
        }
        else{
            _Dissolve();
        }

        switched = !switched;
    }

    private void _Dissolve(){
        foreach(Dissolver d in dissolvers){
            d.DissolveOut();
        }
    }

    private void _UnDissolve(){
        foreach (Dissolver d in dissolvers){
            d.DissolveIn();
        }
    }
}
