﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dissolver : MonoBehaviour {

    public Material dissolveMat;
    public Color edgeColor;
    public string colorString = "";
    public string noiseString = "";
    public string dissovleValueString = "";
    public string edgeThicknessString = "";
    public string textureString = "";

    public float noiseScale = 0;
    public float dissolveValue = 0;
    public float edgeThickness = 0;
    public float speed = 1;
    public Texture feaTexture;
    public Texture ruinTexture;

    public bool dissolveIn = false;
    public bool feaTextureApplied = false;


	// Use this for initialization
	void Start () {
        dissolveMat.SetColor(colorString, edgeColor);
        dissolveMat.SetFloat(noiseString, noiseScale);
        dissolveMat.SetFloat(dissovleValueString, dissolveValue);
        dissolveMat.SetFloat(edgeThicknessString, edgeThickness);
	}
	
	// Update is called once per frame
	void Update () {
        if(dissolveIn){
            if (dissolveValue < 1.5){
                Debug.Log("Disolving in...");
                dissolveValue += Time.deltaTime * speed;
            }
            else if (dissolveValue >= 1.5){
                Debug.Log("Setting texture...");
                SetTexture();
                dissolveIn = !dissolveIn;
            }
        }
        else if(!dissolveIn){
            if (dissolveValue > -1.5){
                Debug.Log("Disolving out...");
                dissolveValue -= Time.deltaTime * speed;
            }
            else if (dissolveValue <= -1.5){

            }
        }
        dissolveMat.SetFloat(dissovleValueString, dissolveValue);
    }

    public void DissolveIn(){
        dissolveIn = true;
    }

    public void DissolveOut(){
        dissolveIn = false;
    }

    public void SetTexture(){
        if (feaTextureApplied){
            if (ruinTexture != null){
                dissolveMat.SetTexture(textureString, ruinTexture);
                try{
                    this.gameObject.GetComponent<BoxCollider>().enabled = true;
                }
                catch (Exception e){
                    Debug.Log(e.Message);
                }
                try{
                    this.gameObject.GetComponent<MeshCollider>().enabled = true;
                }
                catch (Exception e){
                    Debug.Log(e.Message);
                }

                this.gameObject.GetComponent<MeshRenderer>().enabled = true;
            }
            else{
                try{
                    this.gameObject.GetComponent<BoxCollider>().enabled = false;
                }
                catch (Exception e){
                    Debug.Log(e.Message);
                }

                try{
                    this.gameObject.GetComponent<MeshCollider>().enabled = false;
                }
                catch (Exception e){
                    Debug.Log(e.Message);
                }
                this.gameObject.GetComponent<MeshRenderer>().enabled = false;

            }
            feaTextureApplied = false;
        }
        else{
            if (feaTexture != null){
                dissolveMat.SetTexture(textureString, feaTexture);
                this.gameObject.GetComponent<BoxCollider>().enabled = true;
                this.gameObject.GetComponent<MeshRenderer>().enabled = true;
            }
            else
            {
                this.gameObject.GetComponent<BoxCollider>().enabled = false;
                this.gameObject.GetComponent<MeshRenderer>().enabled = false;
            }
            feaTextureApplied = true;
        }
    }
}
