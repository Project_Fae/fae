﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShiftWall : MonoBehaviour {

    private bool isRuined;
    private Renderer rend;

    [SerializeField]
    private Material ruins, clean;

    private void Start()
    {
        rend = GetComponent<Renderer>();
        isRuined = true;
        rend.material = ruins;
    }

    private void OnEnable()
    {
        EventManager.OnShift += WallShift;
    }

    private void OnDisable()
    {
        EventManager.OnShift -= WallShift;
    }

    private void WallShift()
    {
        if (isRuined)
        {
            rend.material = clean;
            isRuined = false;
        } else
        {
            rend.material = ruins;
            isRuined = true;
        }
    }
}
