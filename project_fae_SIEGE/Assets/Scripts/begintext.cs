﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class begintext : MonoBehaviour {
    private Text me;
	// Use this for initialization
	void Start () {
        EventManager.OnShift += Disappear;
        me = this.gameObject.GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void Disappear()
    {
        me.CrossFadeColor(Color.clear, 1f, false, true);
    }
}
