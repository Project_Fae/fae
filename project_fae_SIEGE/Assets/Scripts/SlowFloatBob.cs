﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowFloatBob : MonoBehaviour {

    public GameObject orb;
    public float speed = 2f;
    public float amplitude = 0.1f;

    private float yPos;
    private Vector3 pos;

	// Use this for initialization
	void Start () {
        pos = this.transform.position;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        pos.y += Mathf.Sin(Time.realtimeSinceStartup * speed) * amplitude;
        transform.position = pos;
    }
}
