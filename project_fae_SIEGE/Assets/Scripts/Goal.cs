﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Goal : MonoBehaviour
{
    public Image whiteBox;
    public Text winText;
    bool touched = false;
    Input_Manager im;

    void Awake()
    {
        im = GameObject.FindGameObjectWithTag("Player").GetComponent<Input_Manager>();
        whiteBox.color = Color.clear;
        winText.text = " ";
    }

    void Update()
    {
        if (touched)
        {
            im.EnableMovement(false);
            whiteBox.color = Color.Lerp(whiteBox.color, Color.white, 3.0f * Time.deltaTime);
            winText.color = Color.black;
            winText.text = "YOU WIN" +
                "\nPress Escape to Start Over";
            //StartCoroutine(WinTransition());
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            touched = true;
        }
    }

    //IEnumerator WinTransition()
    //{

    //    yield return new WaitForSeconds(5.0f);
    //    //Cursor.lockState = CursorLockMode.None;
    //    //whiteBox.color = Color.clear;
    //    //winText.text = " ";
    //    SceneManager.LoadScene("MainMenu");
    //}
}
