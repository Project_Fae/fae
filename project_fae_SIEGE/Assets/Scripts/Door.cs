﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour {
    private GameObject me;
    private bool activated = false;
    private Light light1;
    // Use this for initialization
    void Start () {
        light1 = this.transform.GetComponent<Light>();
        me = transform.gameObject;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Switch()
    {
        if (activated == true)
        {
            activated = false;
            me.GetComponent<MeshRenderer>().enabled = true;
            me.GetComponent<Collider>().enabled = true;
            light1.color = Color.red;
            Debug.Log("Door deactivated");
        }
        else
        {
            activated = true;
            me.GetComponent<MeshRenderer>().enabled = false;
            me.GetComponent<Collider>().enabled = false;
            light1.color = Color.blue;
            Debug.Log("Door activated");
        }

    }
}
