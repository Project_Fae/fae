﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NextLevel : MonoBehaviour {

    public Input_Manager player;
    public Collider playerCollider;

    private void OnTriggerEnter(Collider other)
    {
        if (other == playerCollider)
        {
            player.EnablePlayer(false);

            Invoke("LoadNextLevel", 3);
        }
    }

    private void LoadNextLevel()
    {
        SceneManager.LoadScene("Level_1");
    }
}
