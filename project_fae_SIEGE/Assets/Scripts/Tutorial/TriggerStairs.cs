﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerStairs : MonoBehaviour {

    public GameObject player;
    public EventManager eventManager;

    private bool hasLooked = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player && !hasLooked)
        {
            eventManager.LookStairs();
            hasLooked = true;
        }
    }
}
