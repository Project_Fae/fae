﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogManager : MonoBehaviour {

    //Text that will change for each conversation
    public Text nameText;
    public Text dialogText;
    public Image sprite;

    //Text that changes depending on the controller
    public Text continueText;
    public Text skipText;

    //Controller stuff
    public string leftClick;
    public string rightClick;
    public float pressTimer = 0;
    public float pressCycle = 0.8f;

    //Player
    [SerializeField]
    private GameObject player;

    public Animator animator;
    public Input_Manager input;

    public bool isTalking;

    private Queue<Sentence> sentences;

    private void Start(){
        leftClick = input._pickUp;
        rightClick = input._interact;
    }

    // Use this for initialization
    void Awake () {
        sentences = new Queue<Sentence>();
        input = FindObjectOfType<Input_Manager>();
        if (input.controllerPresent)
        {
            continueText.text = "Press Y to continue...";
            skipText.text = "Press Select to skip";
        } else
        {
            continueText.text = "Click to continue...";
            skipText.text = "Right click to skip";
        }
	}

    //Triggering the dialog with a keypress for testing purposes
    private void Update(){
        if(pressTimer > 0){
            pressTimer -= Time.deltaTime;
        }


        if (Input.GetAxis(leftClick) > 0 && pressTimer <= 0){
            pressTimer = pressCycle;
            FindObjectOfType<DialogManager>().DisplayNextSentence();
            Debug.Log("Left clicked!!!!!!!!!!");
        }

        if (Input.GetAxis(rightClick) > 0 && pressTimer <= 0){
            pressTimer = pressCycle;
            FindObjectOfType<DialogManager>().EndDialog();
        }
    }

    public void StartDialog(Dialog dialog)
    {
        //Disable player controls
        input.EnablePlayer(false);

        animator.SetBool("isOpen", true);
        isTalking = true;

        nameText.text = dialog.name;

        if (sentences != null)
        {
            sentences.Clear();
        }

        foreach (Sentence sentence in dialog.sentences)
        {
            sentences.Enqueue(sentence);
        }

        DisplayNextSentence();
    }

    public void DisplayNextSentence()
    {
        if (sentences.Count == 0)
        {
            EndDialog();
            return;
        }

        Sentence sentence = sentences.Dequeue();
        string words = sentence.sentence;
        sprite.sprite = sentence.talkSprite;
        StopAllCoroutines();
        StartCoroutine(TypeSentence(words));
    }

    IEnumerator TypeSentence (string sentence)
    {
        dialogText.text = "";
        foreach (char letter in sentence.ToCharArray())
        {
            dialogText.text += letter;
            yield return null;
        }
    }

    public void EndDialog()
    {
        //Enable player controls
        input.EnablePlayer(true);

        animator.SetBool("isOpen", false);
        isTalking = false;
        
        Debug.Log("End of dialog");
    }

}
