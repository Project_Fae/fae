﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogTrigger : MonoBehaviour {

    public Dialog dialog;

    public void TriggerDialog()
    {
        FindObjectOfType<DialogManager>().StartDialog(dialog);
        Debug.Log("DialogTrigger calling StartDialog");
    }


    ////Triggering the dialog with a keypress for testing purposes
    //private void Update()
    //{

    //    if (Input.GetKeyDown(KeyCode.Mouse0))
    //    {
    //        FindObjectOfType<DialogManager>().DisplayNextSentence();
    //    }

    //    if (Input.GetKeyDown(KeyCode.Mouse1))
    //    {
    //        FindObjectOfType<DialogManager>().EndDialog();
    //    }
    //}
}
