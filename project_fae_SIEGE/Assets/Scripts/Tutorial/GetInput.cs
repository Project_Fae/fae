﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetInput : MonoBehaviour {

    public Input_Manager input;

    public MeshRenderer player;

    public DialogTrigger shiftDialog;

    public DialogTrigger enemyDialog;

    public DialogTrigger stairDialog;

    public GameObject enemy;

    public GameObject instructionPanel;
}
