﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class TriggerDialog1 : MonoBehaviour {

    private DialogManager manager;
    public PlayableDirector swoop;

	// Use this for initialization
	void Start () {
        manager = FindObjectOfType<DialogManager>();
        gameObject.GetComponent<DialogTrigger>().TriggerDialog();
	}
	
	// Update is called once per frame
	void Update () {
		if (!manager.isTalking)
        {
            swoop.Play();
            enabled = false;
        }
	}
}
