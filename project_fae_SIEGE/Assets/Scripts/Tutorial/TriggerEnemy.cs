﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerEnemy : MonoBehaviour {

    public GameObject player;
    public EventManager eventManager;

    private bool hasLooked = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player && !hasLooked)
        {
            eventManager.LookEnemy();
            hasLooked = true;
        }
    }
}
