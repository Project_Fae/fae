﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class TutorialCameraControl : MonoBehaviour
{
    private bool hasShifted;
    private Animator cameraChange;

    public PlayableDirector fpSwoopPlay;

    private void OnEnable()
    {
        //When the player shifts, the FirstShift method runs
        EventManager.OnShift += FirstShift;

        //When the timeline playable for the first person swoop finishes playing, run the FirstSwoop method
        fpSwoopPlay.stopped += FirstSwoop;

        //When the player triggers the EnemyLookTrigger collider, look at the enemy
        EventManager.OnEnemyLook += EnemyLook;

        //When the player triggers the StairLookTrigger, look at where the stairs should be
        EventManager.OnStairLook += StairLook;
    }

    private void OnDisable()
    {
        EventManager.OnShift -= FirstShift;
        fpSwoopPlay.stopped -= FirstSwoop;
        EventManager.OnEnemyLook -= EnemyLook;
        EventManager.OnStairLook -= StairLook;
    }

    // Use this for initialization
    void Start()
    {
        hasShifted = false;

        //Gets the animator used for camera changes
        cameraChange = gameObject.GetComponent<Animator>();

        cameraChange.ResetTrigger("fpEnded");
        cameraChange.ResetTrigger("ShiftBegan");
        cameraChange.ResetTrigger("ShiftEnded");
        cameraChange.ResetTrigger("EnemyEnded");
        cameraChange.ResetTrigger("StairsEnded");
    }

    private void FirstShift()
    {
        if (!hasShifted)
        {
            //Triggers the animation transition for when the player has shifted for the first time
            cameraChange.SetTrigger("ShiftBegan");

            //Sets hasShifted to true, so that this method does not run again
            hasShifted = true;

            if (!FindObjectOfType<DialogManager>().isTalking)
            {
                StartCoroutine(ShiftReturn());
            }
        }
    }

    private void EnemyLook()
    {
        cameraChange.SetTrigger("EnemyBegan");
        if (!FindObjectOfType<DialogManager>().isTalking)
        {
            StartCoroutine(EnemyReturn());
        }
    }

    private void StairLook()
    {
        cameraChange.SetTrigger("StairsBegan");
        if (!FindObjectOfType<DialogManager>().isTalking)
        {
            StartCoroutine(StairReturn());
        }
    }

    //private void ShiftReturnCoroutine()
    //{
    //    StartCoroutine(ShiftReturn());
    //}

    //private void EnemyReturnCoroutine()
    //{
    //    StartCoroutine(EnemyReturn());
    //}

    //private void StairReturnCoroutine()
    //{
    //    StartCoroutine(StairReturn());
    //}

    private IEnumerator ShiftReturn()
    {
        yield return new WaitForSeconds(3);
        cameraChange.SetTrigger("ShiftEnded");
    }

    private IEnumerator EnemyReturn()
    {
        yield return new WaitForSeconds(3);
        cameraChange.SetTrigger("EnemyEnded");
    }

    private IEnumerator StairReturn()
    {
        yield return new WaitForSeconds(3);
        cameraChange.SetTrigger("StairsEnded");
    }

    private void FirstSwoop(PlayableDirector swoop)
    {
        //When the first person swoop finishes playing, this triggers the animation transition to first person view
        cameraChange.SetTrigger("fpEnded");
    }
}