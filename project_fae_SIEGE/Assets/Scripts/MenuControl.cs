﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuControl : MonoBehaviour
{
    public GameObject startButton;
    public GameObject quitButton;
    public GameObject text;
    public RawImage art;
    public RawImage title;
    public LoadCanvas canvas;

    bool showCredits = false;

    public void StartGame()
    {
        //canvas.EnableCanvas(true);
        SceneManager.LoadScene("Tutorial");
    }

    public void EndGame()
    {
        Application.Quit();
    }

    public void Credits()
    {
        showCredits = !showCredits;
        GUIManager();
    }

    void GUIManager()
    {
        if(showCredits == true)
        {
            startButton.gameObject.SetActive(false);
            quitButton.gameObject.SetActive(false);
            art.enabled = false;
            title.enabled = false;

            text.gameObject.SetActive(true);
        }

        if (showCredits == false)
        {
            startButton.gameObject.SetActive(true);
            quitButton.gameObject.SetActive(true);
            art.enabled = true;
            title.enabled = true;

            text.gameObject.SetActive(false);
        }
    }
}
