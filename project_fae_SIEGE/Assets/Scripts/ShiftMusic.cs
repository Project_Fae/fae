﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShiftMusic : MonoBehaviour {

    private bool isRuined;

    [SerializeField]
    private AudioSource ruinAudio, cleanAudio, blonk;

    private void Start()
    {
        isRuined = true;
        ruinAudio.volume = 0.6f;
        cleanAudio.volume = 0.0f;
    }

    private void OnEnable()
    {
        EventManager.OnShift += AudioShift;
    }

    private void OnDisable()
    {
        EventManager.OnShift -= AudioShift;
    }

    private void AudioShift()
    {
        if (!isRuined)
        {
            ruinAudio.volume = 0.6f;
            cleanAudio.volume = 0.0f;
            isRuined = true;
        } else
        {
            ruinAudio.volume = 0.0f;
            cleanAudio.volume = 0.6f;
            isRuined = false;
        }
        blonk.Play();
    }
}
