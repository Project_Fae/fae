﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EyeRotate : MonoBehaviour {
    private GameObject me;
    private bool rotating;
    private float timer;
	// Use this for initialization
	void Start () {
        EventManager.OnShift += ProcessEyes;
        me = this.gameObject;
	}

    // Update is called once per frame
    IEnumerator Rotate(Vector3 byAngles, float inTime)
    {
        var fromAngle = transform.rotation;
        var toAngle = Quaternion.Euler(transform.eulerAngles + byAngles);
        for (var t = 0f; t < 1; t += Time.deltaTime / inTime)
        {
            transform.rotation = Quaternion.Slerp(fromAngle, toAngle, t);
            yield return null;
        }
    }
    void Update()
    {

    }
    public void ProcessEyes()
    {
        Image Eyetransp = me.GetComponent<Image>();
        me.GetComponent<Image>().color = new Color(Eyetransp.color.r, Eyetransp.color.g, Eyetransp.color.b, 1);
        Debug.Log(Eyetransp.color.ToString());
        StartCoroutine(Rotate(Vector3.forward * 180, 0.3f));
        StartCoroutine(Fade(Eyetransp, 1f));
        Debug.Log(Eyetransp.ToString());

    }

    IEnumerator Fade(Image m, float inTime)
    {
        var fromColor = me.GetComponent<Image>().color;

        for (var t = 0f; t < 1; t += Time.deltaTime / inTime)
        {
            me.GetComponent<Image>().color = Color.Lerp(me.GetComponent<Image>().color, new Color(m.color.r, m.color.g, m.color.b, 0), t);
            yield return null;
        }

    }
}
