﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadCanvas : MonoBehaviour {

    public Scene canvasScene;
    public Scene mainMenuScene;

    public GameObject gameCanvas;
    GameObject[] gameObjects;

	// Use this for initialization
	void Start () {
        canvasScene = SceneManager.GetSceneByBuildIndex(3);
        mainMenuScene = SceneManager.GetActiveScene();


        int numScenes = SceneManager.sceneCount;
        if(numScenes < 2){
            SceneManager.LoadScene(3, LoadSceneMode.Additive);
        }

        gameCanvas = GameObject.FindWithTag("Game Canvas");
        if(gameCanvas == null){
            Debug.Log("Couldn't find canvas...");
        }
        else{
            Debug.Log("Found canvas: " + gameCanvas.name);
        }

        //gameObjects = SceneManager.GetSceneByBuildIndex(3).GetRootGameObjects();
        //foreach(GameObject go in gameObjects){
        //    if (go.name == "Canvas"){
        //        gameCanvas = go;
        //    }
        //    else{
        //        gameCanvas = GameObject.FindWithTag("Game Canvas");
        //    }
        //}


    }

    public void EnableCanvas(bool b){
        gameCanvas.SetActive(b);
    }
}
