﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShiftRed : MonoBehaviour {

    private bool redActive;

    private void Start()
    {
        redActive = true;
        foreach(Transform child in transform)
        {
            child.gameObject.SetActive(true);
        }
    }

    private void OnEnable()
    {
        EventManager.OnShift += RedShift;
        Debug.Log("RedShift");
    }

    private void OnDisable()
    {
        EventManager.OnShift -= RedShift;
    }

    void RedShift ()
    {
        if (redActive)
        {
            foreach (Transform child in transform)
            {
                child.gameObject.SetActive(false);
            }
            redActive = false;
        } else
        {
            foreach (Transform child in transform)
            {
                child.gameObject.SetActive(true);
            }
            redActive = true;
        }
    }
}
