﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Movement_Transform_Field : MonoBehaviour {

    //Public members
    public Transform[] travelPoints = new Transform[15];
    public GameObject playerGO;
    public AudioClip angeryScream;
    public AudioClip calmEnemyNoise;
    public AudioClip angeryEnemyNoise;
    public AudioSource currentEnemyNoise;
    public int angerLevel = 0;
    public float waitTimerCycle = 0.2f;
    public float speed = 1;
    public float angerTimer = 0;
    public float anger1 = 20;
    public float anger2 = 30;
    public float moveTimerCycle = 0.2f;
    public bool aware = false;
    public bool screamed1 = false;
    public bool screamed2 = false;

    //Private members
    private System.Random _rnd = new System.Random();
    private float _waitTimer = 0;
    [SerializeField]
    public Vector3 _target = new Vector3();
    [SerializeField]
    private bool _moving = false;
    [SerializeField]
    private float _moveTimer = 0;
    [SerializeField]
    private float _playerDistance = 0;
    private bool _getAngery = true;

	// Use this for initialization
	void Start () {
		
	}

    private void Update(){
        if (angerLevel == 0 && _getAngery){
            _TimeMovement();
            _Wander();
        }
        else if (angerLevel == 1 || !_getAngery){
            _FollowPlayer();
        }
        else if(angerLevel == 2 && _getAngery){
            _GoToPlayer();
        }

        if (_getAngery){
            _TimeAnger();
        }

        if (aware){
            _TrackPlayer();
        }
    }

    public virtual void DoStuff(){
        if (angerLevel == 0){
            if (_moveTimer > 0){
                _moveTimer -= Time.deltaTime;
            }

            if (_moveTimer <= 0){
                _moveTimer = moveTimerCycle;
                _Search();
            }
            _Wander();
        }

        if (aware){
            _TrackPlayer();
            _TimeAnger();
        }
    }

    private void _TimeAnger(){
        angerTimer += Time.deltaTime;

        if (angerTimer > anger2){
            angerLevel = 2;
            if (!screamed2){
                AudioSource.PlayClipAtPoint(angeryScream, this.transform.position);
                screamed2 = true;
            }

        }
        else if(angerTimer > anger1){
            angerLevel = 1;
            if (!screamed1){
                AudioSource.PlayClipAtPoint(angeryScream, this.transform.position);
                currentEnemyNoise.clip = angeryEnemyNoise;
                screamed1 = true;
            }
        }
    }

    private void _TimeMovement(){
        if (_moveTimer > 0){
            _moveTimer -= Time.deltaTime;
        }

        if (_moveTimer <= 0){
            _moveTimer = moveTimerCycle;
            _Search();
        }
    }

    private void _FindPlayer(){
        Vector3 currentLocation = this.transform.position;
        Vector3 playerLocation = playerGO.transform.position;
        RaycastHit hit;

        Physics.Raycast(currentLocation, playerLocation, out hit);

        if(hit.transform == null || hit.transform.gameObject.tag == "Player"){
            aware = true;
        }
    }

    private void _Search(){
        _waitTimer = waitTimerCycle;
        int num = _rnd.Next(0, travelPoints.Length);
        Vector3 lookLocation = travelPoints[num].position;
        Vector3 currentLocation = this.transform.position;

        _target = lookLocation;
        _moving = true;
        transform.LookAt(_target);


        //RaycastHit hit;

        //Physics.Raycast(currentLocation, lookLocation, out hit);




        //if (hit.transform == null){
        //    _target = lookLocation;
        //    transform.LookAt(_target);
        //}
    }

    private void _TrackPlayer(){
        _playerDistance = Vector3.Distance(playerGO.transform.position, this.transform.position);
    }

    private void _Wander(){
        if(aware && _playerDistance > 3){
            transform.LookAt(playerGO.transform);
        }
        transform.position += transform.forward * speed * Time.deltaTime;
    }

    private void _FollowPlayer(){
        if (_playerDistance > 1.5){
            transform.LookAt(playerGO.transform);
        }
        transform.position += transform.forward * speed * Time.deltaTime;
    }

    private void _GoToPlayer(){
        transform.LookAt(playerGO.transform);
        transform.position += transform.forward * speed * Time.deltaTime;
    }

    private void OnCollisionEnter(Collision collision){
        _moveTimer = 0;
    }

    /// <summary>
    /// Toggles on and off wether or not the enemy will get angery at the 
    /// player or just follow the player.
    /// </summary>
    /// <param name="b">If set to <c>true</c> b.</param>
    public void EnableAnger(bool b){
        _getAngery = b;
    }
}
