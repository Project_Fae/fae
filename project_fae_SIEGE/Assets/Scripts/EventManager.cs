﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EventManager : MonoBehaviour {
    public delegate void WorldShift();
    public delegate void EnemyLook();
    public delegate void StairLook();
    public static event WorldShift OnShift;
    public static event EnemyLook OnEnemyLook;
    public static event StairLook OnStairLook;
    public float cooldown;
    public float nextfire;

    public void Shift(){
        Debug.Log("Shifting...");
        nextfire = Time.time + cooldown;
        if (OnShift != null)
        {
            OnShift();
            Debug.Log("OnShift");
        }
    }

    public void LookEnemy()
    {
        if (OnEnemyLook != null)
        {
            OnEnemyLook();
        }
    }

    public void LookStairs()
    {
        if (OnStairLook != null)
        {
            OnStairLook();
        }
    }
}
